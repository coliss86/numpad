# Numpad ⌨️

A 5*4 numpad handwired with a 3d printed case.

![Numpad](doc/numpad.jpg)

## Layout 👗👔

In the `qmk/` folder is the complete keyboard layout and instruction to flash it.
The layout is simple: a based keypad with a tap dance on the upper lest key to activate a `FUNC` layer.

## 3D Case 🔧

I printed the case from [thingiverse](https://www.thingiverse.com/thing:3682168) on an Ender 3.

![Case](doc/case.jpg)

It took about 4h30 to print with 0.32 layer at 70 mm/s. The filament used is [PLA bling grey](https://www.filimprimante3d.fr/filament-pla-bling/2816-pla-bling-175-mm-argent-dailyfil-05kg.html).

## Electronic 📟

This numpad is handwired, it took me about 2 hours to solder everything. It is based on a Arduino Promicro.
I followed this [guide to solder everything](https://geekhack.org/index.php?topic=87689.0).

## BOM 🧾

- [ ] 17 * Cherry MX Switches (I used some Gatheron Red)
- [ ] 3 * 2U stabs (optional)
- [ ] 17 * 1N4148 diode
- [ ] 1 * Pro Micro
- [ ] 1 * Micro USB cable (I used a [magnetic one](https://www.aliexpress.com/item/4000720530970.html), very useful)
- [ ] Wire (I salvaged an old round IDE cable)

![Case](doc/handwired.jpg)
