/* Copyright 2021 Coliss86
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

#include QMK_KEYBOARD_H

enum layers {
    _NUM = 0,
    _FUNC,
};

enum custom_keycodes {
    CALC = SAFE_RANGE,
};

#define LR_FUNC  LT(_FUNC, KC_ESC)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_NUM] = LAYOUT(
        LR_FUNC, KC_PSLS, KC_PAST, KC_PMNS,
        KC_P7,   KC_P8,   KC_P9,   KC_PPLS,
        KC_P4,   KC_P5,   KC_P6,
        KC_P1,   KC_P2,   KC_P3,   KC_PENT,
                 KC_P0,   KC_PDOT
    ),

    [_FUNC] = LAYOUT(
        KC_NO,   _______, _______, RESET,
        _______, _______, _______, KC_NLCK,
        _______, _______, _______,
        _______, _______, _______, CALC,
                 _______, KC_BSPC
    ),
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
    case CALC:
        if (record->event.pressed) {
            SEND_STRING( SS_DOWN( X_LGUI ) SS_TAP( X_SPACE ) SS_UP( X_LGUI ) );
            _delay_ms( 100 );
            SEND_STRING("cqlculqtor"  SS_TAP( X_ENTER ) );
        }
        break;
    }
    // Let QMK process the keycode as usual
    return true;
}
