## Instructions

See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Import QMK firmware

```
git clone https://github.com/qmk/qmk_firmware
git clone https://gitlab.com/coliss86/numpad
cd qmk_firmware
git checkout 0.12.34
cd keyboards
ln -s ../../numpad/qmk numpad
```

To update `qmk_firmware`
```
git fetch -a
git checkout <new_tag>
```

## Flash the Atmega
To build the firmware, launch the following command in the root of qmk folder after setting up your build environment :

```
make numpad:default
```

To upload to your `atmega32u4`, hit twice in a second the reset switch and launch :
```
make numpad:flash
```
